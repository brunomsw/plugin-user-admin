<?php
namespace UserAdmin\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use UserAdmin\Model\Entity\Group;

/**
 * Groups Model
 */
class GroupsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('users_groups');
        $this->displayField('name');
        $this->primaryKey('id');
        $this->hasMany('Users', [
                'foreignKey' => 'group_id',
                'className' => 'UserAdmin.Users',
                'dependent' => true
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('status', 'valid', ['rule' => 'numeric'])
            ->requirePresence('status', 'create')
            ->notEmpty('status')
            ->add('removed', 'valid', ['rule' => 'numeric']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));

        return $rules;
    }

    /**
     * Grupos válidos são os grupos que não foram removidos (users_groups.removed == 0).
     * @return [type] [description]
     */
    public function getAllValidGroups()
    {
        return $this->find()->where(['Groups.removed' => 0])->all();
    }

    public function getAllValidGroupsAsList()
    {
        return $this->find('list', ['keyField' => 'id', 'valueField' => 'name'])->where(['Groups.removed' => 0])->toArray();
    }

    public function newGroup($name, $status)
    {
        $group = $this->newEntity(['name' => $name, 'status' => $status]);
        return $this->save($group);
    }

}
