<?php
namespace UserAdmin\Model\Entity;

use Cake\ORM\Entity;

/**
 * Group Entity.
 */
class Group extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'status' => true,
        'removed' => true,
    ];

    public function getStatusAsString()
    {
        switch($this->status) {
            case 1:
                return 'Ativo';
            case 2:
                return 'Inativo';
            default:
                return 'Inválido / Não definido';
        }
    }

    public function getUsers()
    {
        $usersTable = TableRegistry::get('UserAdmin.Users');
        return $usersTable->find()->where(['Users.group_id' => $this->id])->all();
    }
}
