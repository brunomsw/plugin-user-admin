<?php
namespace UserAdmin\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity.
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'password' => true,
        'group_id' => true,
        'active' => true,
        'removed' => true
    ];


    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

    public function getStatusAsString()
    {
        switch($this->active) {
            case 1:
                return 'Ativo';
            case 2:
                return 'Inativo';
            default:
                return 'Inválido / Não definido';
        }
    }

}
