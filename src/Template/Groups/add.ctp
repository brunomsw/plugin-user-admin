<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Novo Grupo</h1>
		<p>Preencha o formulário para cadastrar um novo usuário</p>
		<?php
			echo $this->Flash->render();

			echo $this->Form->create($group);

			echo $this->Form->input('name', ['label' => 'Nome']);

			echo $this->Form->checkbox('status', ['label' => 'Ativo']);

			echo $this->Form->submit('Cadastrar', ['class' => 'btn btn-primary']);
			echo $this->Form->end();
		?>
	</div>
</div>

