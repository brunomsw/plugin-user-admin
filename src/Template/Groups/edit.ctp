<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Editar Grupo: <?= $group->name ?></h1>
		<p>Preencha as informações e clique em 'Editar'</p>
		<?php
			echo $this->Flash->render();

			echo $this->Form->create($group);

			echo $this->Form->input('name', ['label' => 'Nome']);

			echo $this->Form->checkbox('status', ['label' => 'Ativo']);

			echo $this->Form->submit('Editar', ['class' => 'btn btn-primary']);
			echo $this->Form->end();
		?>
	</div>
</div>
