<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Grupos Cadastrados <?= $this->Html->link('Novo Grupo', '/interno/usuarios/grupos/novo', ['class' => 'btn btn-primary btn-lg']) ?></h1>
		<?php
			echo $this->Flash->render();

			echo $this->Html->tag('table', null, ['class' => 'table stripped table-bordered realties-table']);
			
			echo $this->Html->tag('thead', $this->Html->tableHeaders(['Login', 'Status','Opções']));
			$cells = [];
			foreach($groups as $group){
				$options = [];

				$options[] = $this->Html->link('Editar', '/interno/usuarios/grupos/editar/' . $group->id, ['class' => 'btn btn-sm btn-primary']);
				$options[] = $this->Html->link('Excluir', '/interno/usuarios/grupos/remover/' . $group->id, ['class' => 'btn btn-sm btn-danger', 'confirm' => 'Tem certeza que deseja excluir o grupo ' . $group->username . '?']);
				$cells[] = [$group->name, $group->getStatusAsString(), implode(' ', $options)];
			}

			if(!empty($cells))
				echo $this->Html->tableCells($cells);
			else
				echo $this->Flash->render('notice');

			echo $this->Html->tag('/table');
		?>
	</div>
</div>