<?php

if($this->request->session()->read('Auth.User.is_root')) {
	$menuItems = [
		['title' => 'Administrar Usuários', 'url' => '/interno/usuarios', 'icon' => ''],
		['title' => 'Administrar Grupos', 'url' => '/interno/usuarios/grupos', 'icon' => '']
	];
	
	echo $this->PanelMenu->createSecondLevelMenuTree('Controle de Acesso', 'key', $menuItems);
}
