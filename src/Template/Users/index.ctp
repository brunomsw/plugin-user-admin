<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Usuários Cadastrados <?= $this->Html->link('Novo Usuário', '/interno/usuarios/novo', ['class' => 'btn btn-primary btn-lg']) ?></h1>
		<?php
			echo $this->Flash->render();

			echo $this->Html->tag('table', null, ['class' => 'table stripped table-bordered realties-table']);

			echo $this->Html->tag('thead', $this->Html->tableHeaders(['Login', 'Status','Opções']));
			$cells = [];
			foreach($users as $user){
				$cell = [];

				$cell[] = $user->username;

				$cell[] = $user->getStatusAsString();
				$cell[] = implode([
						$this->Html->link('Editar', '/interno/usuarios/editar/' . $user->id, ['class' => 'btn btn-sm btn-primary']),
						$this->Html->link('Excluir', '/interno/usuarios/remover/' . $user->id, ['class' => 'btn btn-sm btn-danger', 'confirm' => 'Tem certeza que deseja excluir o usuário ' . $user->username . '?'])
					], ' ');
					$cells[] = $cell;
			}

			if(!empty($cells))
				echo $this->Html->tableCells($cells);
			else
				echo $this->Flash->render('notice');

			echo $this->Html->tag('/table');
		?>
	</div>
</div>
