<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Editar Usuário: <?= $user->username ?></h1>
		<p>Preencha o formulário para editar o usuário</p>
		<?php
			echo $this->Flash->render();

			echo $this->Form->create($user, ['autocomplete' => 'off']);

			echo $this->Form->input('username', ['label' => 'Login']);

			echo $this->Form->input('password', ['label' => 'Senha', 'value' => '', 'placeholder' => 'Nova senha (deixe em branco se não for alterá-la)']);


			if($amIRoot) {
				echo $this->Form->select('group_id', $groups, ['label' => 'Grupo']);
			}

			echo $this->Form->checkbox('active', ['label' => 'Ativo']);

			echo $this->Form->submit('Editar', ['class' => 'btn btn-primary']);
			echo $this->Form->end();
		?>
	</div>
</div>
