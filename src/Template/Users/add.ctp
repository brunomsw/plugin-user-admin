<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Novo Usuário</h1>
		<p>Preencha o formulário para cadastrar um novo usuário</p>
		<?php
			echo $this->Flash->render();

			echo $this->Form->create($user);

			echo $this->Form->input('username', ['label' => 'Login']);

			echo $this->Form->input('password', ['label' => 'Senha']);

			echo $this->Form->select('group_id', $groups, ['label' => 'Grupo']);

			echo $this->Form->checkbox('active', ['label' => 'Ativo']);

			echo $this->Form->submit('Cadastrar', ['class' => 'btn btn-primary']);
			echo $this->Form->end();
		?>
	</div>
</div>

