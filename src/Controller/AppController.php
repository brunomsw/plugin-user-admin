<?php

namespace UserAdmin\Controller;

use ControlPanel\Controller\AppController as BaseController;
use Cake\Core\Configure;

class AppController extends BaseController
{

	public function initialize(){
		parent::initialize();
		$this->theme = Configure::read('WebImobApp.Plugins.UserAdmin.Settings.Template.all.theme');
		$this->layout = Configure::read('WebImobApp.Plugins.UserAdmin.Settings.Template.all.layout');
		$this->Auth->config('authorize', ['Controller']);
		$this->Auth->config('unauthorizedRedirect', '/interno');
		$this->Auth->config('authError', 'Acesso Negado. Somente administradores tem acesso as informações de controle de acesso.');
	}
}
