<?php
namespace UserAdmin\Controller;

use UserAdmin\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Users Controller
 *
 * @property \UserAdmin\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
	public $helpers = ['AppCore.Form', 'DefaultAdminTheme.PanelMenu'];


	public function beforeFilter(Event $event){
    $this->Groups = TableRegistry::get('UserAdmin.Groups');
    $this->Users = TableRegistry::get('UserAdmin.Users');
	}

	public function initialize()
	{
		parent::initialize();
		if($this->Auth->user('id') == $this->request->params['id'])
			$this->Auth->allow('edit');
	}
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
      $this->set('users', $this->Users->getAllUsers());
    }

    public function add()
		{
    	if($this->request->is('post')) {
        $data = $this->request->data;
    		$newUser = $this->Users->newEntity($data);

    		if($this->Users->save($newUser)) {
   				$this->Flash->set('Usuário criado com sucesso', ['element' => 'alert_success']);
          $this->request->data = [];
    		}
    		else {
    			$this->Flash->set('Erro.', ['element' => 'alert_danger']);
    		}
    	}
      $this->set('user', $this->Users->newEntity());
      $this->set('groups', $this->Groups->getAllValidGroupsAsList());
    }

    public function edit($id)
		{
			if($this->request->is('post')) {
        $data = $this->request->data;

				if(empty($data['password']))
					unset($data['password']);

				if(!$this->Auth->user('is_root') and isset($data['group_id']))
					unset($data['group_id']);

    		$user = $this->Users->get($id);
				$user = $this->Users->patchEntity($user, $data);

    		if($this->Users->save($user)) {
   				$this->Flash->set('Usuário alterado com sucesso', ['element' => 'alert_success']);
          $this->request->data = [];
    		}
    		else {
    			$this->Flash->set('Erro.', ['element' => 'alert_danger']);
    		}
    	}

			$this->set('amIRoot', $this->Auth->user('is_root'));
			$this->set('user', $this->Users->get($id));
      $this->set('groups', $this->Groups->getAllValidGroupsAsList());
    }

    public function delete($id) {
      $user = $this->Users->get($id);
      $r = $this->Users->delete($user);

      if($r)
        $this->Flash->set('Usuário removido com sucesso', ['element' => 'alert_success']);
      else
        $this->Flash->set('Houve um erro ao tentar remover o usuário', ['element' => 'alert_danger']);
      $this->redirect(['action' => 'index']);
    }
}
