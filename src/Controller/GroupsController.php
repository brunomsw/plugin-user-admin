<?php
namespace UserAdmin\Controller;

use UserAdmin\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * Groups Controller
 *
 * @property \UserAdmin\Model\Table\GroupsTable $Groups
 */
class GroupsController extends AppController
{
    public $helpers = ['AppCore.Form', 'DefaultAdminTheme.PanelMenu'];

    public function initialize() {
        parent::initialize();
        $this->Groups = TableRegistry::get('UserAdmin.Groups');
        $this->Users = TableRegistry::get('UserAdmin.Users');
    }

    public function index() {
        $this->set('groups', $this->Groups->getAllValidGroups());
    }

    public function add() {
        if($this->request->is('post')) {
            $data = $this->request->data;

            $group = $this->Groups->newEntity($data);
            $group = $this->Groups->save($group);
            if($group) {
                $this->Flash->set('Grupo criado.', ['element' => 'alert_success']);
                $this->request->data = [];
            }
            else {
                $this->Flash->set('Não foi possivel adicionar o grupo.', ['element' => 'alert_danger']);
            }
        }

        $this->set('group', $this->Groups->newEntity());
    }

    public function edit($id)
    {
      if($this->request->is('post')) {
        $data = $this->request->data;

        $group = $this->Groups->get($id);
        $group = $this->Groups->patchEntity($group, $data);

        if ($this->Groups->save($group)) {
          $this->Flash->set('Grupo editado.', ['element' => 'alert_success']);
          $this->request->data = [];
        }
        else {
          $this->Flash->set('Não foi possivel editar o grupo. Nome do grupo já existe?', ['element' => 'alert_danger']);
        }
      }
      $this->set('group', $this->Groups->get($id));
    }

    public function delete($id) {
        $group = $this->Groups->get($id);

        $group->removed = 1;
        $group->status = 0;

        if($this->Groups->save($group))
            $this->Flash->set('Grupo deletado', ['element' => 'alert_success']);
        else
            $this->Flash->set('Não foi possivel deletar o grupo.', ['element' => 'alert_danger']);
        $this->redirect('/interno/usuarios/grupos');
    }
}
