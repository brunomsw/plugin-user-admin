<?php
namespace UserAdmin\Controller;

use UserAdmin\Controller\AppController;
use Cake\Core\Configure;

/**
 * Authenticate Controller
 *
 * @property \UserAdmin\Model\Table\AuthenticateTable $Authenticate
 */
class AuthenticateController extends AppController
{
    public $helpers = ['AppCore.Form', 'DefaultAdminTheme.PanelMenu'];

    public function initialize(){
        parent::initialize();
        $this->theme = Configure::read('WebImobApp.Plugins.UserAdmin.Settings.Template.Authenticate.theme');
        $this->layout = Configure::read('WebImobApp.Plugins.UserAdmin.Settings.Template.Authenticate.layout');

        $this->Auth->config('authorize', false);
    }

    public function login() {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $user['is_root'] = false;

                $this->loadModel('UserAdmin.Groups');
                $group = $this->Groups->get($user['group_id']);
                if($group->name === Configure::read('WebImobApp.Plugins.UserAdmin.Settings.Authorization.root_group_name'))
                    $user['is_root'] = true;
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->set('Usuário não encontrado', ['element' => 'alert_danger', 'key' => 'auth']);
            }
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }


}
