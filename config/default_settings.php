<?php

use Cake\Core\Configure;

return [
	'WebImobApp.Plugins.UserAdmin.Settings' => [
		'General' => ['display_panel_menu' => true],
		'Template' => [
			'all' => [
				'layout' => Configure::read('WebImobApp.Plugins.ControlPanel.Settings.Template.layout'),
				'theme' => Configure::read('WebImobApp.Plugins.ControlPanel.Settings.Template.theme'),
			],
			'Authenticate' => [
				'layout' => 'login',
        'theme' => Configure::read('WebImobApp.Plugins.ControlPanel.Settings.Template.theme')
			],
		],
		'Authorization' => [
			'root_group_name' => 'root'
		]
	]
];
