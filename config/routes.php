<?php
use Cake\Routing\Router;

Router::plugin('UserAdmin', ['path' => '/interno'], function ($routes) {
	$routes->connect('/login', ['plugin' => 'UserAdmin','controller' => 'Authenticate', 'action' => 'login']);
	$routes->connect('/logout', ['plugin' => 'UserAdmin','controller' => 'Authenticate', 'action' => 'logout']);

	$routes->connect('/usuarios', ['plugin' => 'UserAdmin','controller' => 'Users', 'action' => 'index']);
	$routes->connect('/usuarios/novo', ['plugin' => 'UserAdmin','controller' => 'Users', 'action' => 'add']);
	$routes->connect('/usuarios/editar/:id', ['plugin' => 'UserAdmin','controller' => 'Users', 'action' => 'edit'], ['pass' => ['id']]);
	$routes->connect('/usuarios/remover/:id', ['plugin' => 'UserAdmin','controller' => 'Users', 'action' => 'delete'], ['pass' => ['id']]);

	$routes->connect('/usuarios/grupos', ['plugin' => 'UserAdmin','controller' => 'Groups', 'action' => 'index']);
	$routes->connect('/usuarios/grupos/novo', ['plugin' => 'UserAdmin','controller' => 'Groups', 'action' => 'add']);
	$routes->connect('/usuarios/grupos/editar/:id', ['plugin' => 'UserAdmin','controller' => 'Groups', 'action' => 'edit'], ['pass' => ['id']]);
	$routes->connect('/usuarios/grupos/remover/:id', ['plugin' => 'UserAdmin','controller' => 'Groups', 'action' => 'delete'], ['pass' => ['id']]);
});
